FROM python:latest
WORKDIR /usr/src/app
COPY testadd.py /usr/src/app
EXPOSE 8080
ENTRYPOINT ["python"]
CMD  ["testadd.py"]
